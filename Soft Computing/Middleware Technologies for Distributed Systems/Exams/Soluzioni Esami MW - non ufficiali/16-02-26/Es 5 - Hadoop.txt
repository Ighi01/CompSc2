1 Job: (1 Map - 1 Reduce)

job1.setInputFormat(TextInputFormat.class);
job1.setOutputFormat(TextOutputFormat.class);
job1.setOutputKeyClass(IntWritable.class);
job1.setOutputValueClass(Text.class);

public void map(LongWritable key, Text value, OutputCollector<IntWritable, IntWritable> output, Reporter reporter);
public void reduce(IntWritable key, Iterator<IntWritable> values, OutputCollector<IntWritable, IntWritable> output, Reporter reporter);

Mapper:
	We don't care about the input key. In the input value there are all the needed information separated by a comma, so we split them, and then we check that:
		starttime is greater than or equal to 01/06;
		stoptime is less than or equal to 31/08;
		start station id is equal to 001;
		end station id is equal to 002;
		bith year of user between 1978 and 1999.
	
	If all these condition are checked so we collect to output:
		key -> 1 (just a flag)
		value -> (stoptime - starttime)

Reducer:
	We don't care the input key. While we have values we increment a counter and we sum all these value.
	At the end we simply output:
		key -> input key
		value -> sum/counter (so we have the median value)
