% Orion, Prolog variant - MPradella, MMXII

%% datatype: we will use the term sfloat(X,Y,Z)
valid(sfloat(X,X,_), sfloat(X,X,X)).
valid(sfloat(X,_,X), sfloat(X,X,X)).
valid(sfloat(_,Y,Y), sfloat(Y,Y,Y)).


sfadd(sfloat(X1,Y1,Z1),sfloat(X2,Y2,Z2), Z) :-
	X0 is X1+X2, Y0 is Y1+Y2, Z0 is Z1+Z2,
	valid(sfloat(X0,Y0,Z0),Z).

sfsub(sfloat(X1,Y1,Z1),sfloat(X2,Y2,Z2), Z) :-
	X0 is X1-X2, Y0 is Y1-Y2, Z0 is Z1-Z2,
	valid(sfloat(X0,Y0,Z0),Z).

sfmul(sfloat(X1,Y1,Z1),sfloat(X2,Y2,Z2), Z) :-
	X0 is X1*X2, Y0 is Y1*Y2, Z0 is Z1*Z2,
	valid(sfloat(X0,Y0,Z0),Z).

sfdiv(sfloat(X1,Y1,Z1),sfloat(X2,Y2,Z2), Z) :-
	X0 is X1/X2, Y0 is Y1/Y2, Z0 is Z1/Z2,
	valid(sfloat(X0,Y0,Z0),Z).