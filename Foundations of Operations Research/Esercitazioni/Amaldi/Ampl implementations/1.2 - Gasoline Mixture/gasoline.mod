/*************gasoline.mod************/

set J;		#set of types of gasoline
set I;		#set of types of oil

param q_max{I,J} default 1;		#maximum quantity (%) of oil of type i in gasoline j
param q_min{I,J} default 0;		#minimum quantity (%) of oil of type i in gasoline j

param p{J};			#profit (�/barrel) from j-th gasoline type
param a{I};			#availability of i-th type of oil (in barrels)
param c{I};			#cost of the i-th type of oil (�/barrel)

var x{J} >= 0; 		#number of barrels of gasoline of type j produced
var y{I,J} >= 0;	#number of barrels of oil of type i bought in order to make gasoline j

maximize revenue:
	sum{j in J}( p[j]*x[j] ) - sum{i in I, j in J}( c[i]*y[i,j] );
	
subject to availability{i in I}:
	sum{j in J}(y[i,j]) <= a[i];

subject to mixtureRuleMax{j in J, i in I}:
	y[i,j] <= q_max[i,j]*x[j];

subject to mixtureRuleMin{j in J, i in I}:
	y[i,j] >= q_min[i,j]*x[j];

subject to conservation{j in J}:
	x[j] = sum{i in I}(y[i,j]);

data;

set J:= A, B;
set I:= O1, O2, O3;

param q_max:=
	O1	A	0.3,
	O1	B	0.5;

param q_min:=
	O2	A	0.4,
	O2	B	0.1;

param a:=
	O1	3000,
	O2	2000,
	O3	4000;

param c:=
	O1	3,
	O2	6,
	O3	4;

param p:=
	A	5.5,
	B	4.5;