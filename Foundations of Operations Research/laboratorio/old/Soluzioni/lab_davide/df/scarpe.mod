# SETS

param n;
set I := 1..n;

param m;
set J := 1..m;

# PARAMS

param t{I,J};
param p{J};
param q{J};
param T_min;
param T_max;

var x{I,J} >= 0, integer;
var y{j in J} >= 0, <= q[j], integer;

maximize profitto:
	sum{j in J} p[j] * y[j];

subject to max_ore{i in I}:
	sum{j in J} x[i,j] <= T_max;

subject to min_ore{i in I}:
	sum{j in J} x[i,j] >= T_min;

subject to produzione{j in J}:
	y[j] = sum{i in I} x[i,j]/t[i,j];



